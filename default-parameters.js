function sum(x, y = 12) {
    // y is 12 if not passed (or passed as undefined)
    console.log(x + y);
  }
  sum(3); //15
  sum(3, 6); //9
// Spread Syntax in object literals
const obj1 = { property1: 'hello', x: 42 };
const obj2 = { property1: 'helloAnother', y: 13 };

const clonedObj = { ...obj1 };
console.log('Cloned object :',clonedObj);
// Object { property1: "hello", x: 42 }

const mergedObj = { ...obj1, ...obj2 };
console.log('Merged object:', mergedObj);
// Object { property1: "helloAnother", x: 42, y: 13 }

/**
 * To read more about spread syntax
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Spread_syntax#Spread_in_object_literals
 *  */ 
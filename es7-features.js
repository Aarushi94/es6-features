console.log('-----Array.prototype.includes------');
console.log(['a', 'b', 'c'].includes('a')); // true, not 0 like indexOf would give
console.log(['a', 'b', 'c'].includes('d')); // false

console.log('-----Exponentiation------');
// 2 to the power of 8
console.log(Math.pow(2, 8)); // 256
console.log(2 ** 8);

console.log('-----Object.values------');
console.log(Object.values({ 'a': 23, 'b': 19 })); // [23, 19]
// Array-like object (order not preserved)
console.log(Object.values({ 80: 'eighty', 0: 1, 1: 'yes' })) // [1, 'yes', 'eighty']

console.log('-------Async Await------');
function resolveAfter2Seconds() {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve('resolved');
      }, 2000);
    });
  }
  // using promise we had to handle .then() & .catch()
  resolveAfter2Seconds().then((result)=>{
    console.log('using .then() to get result:', result);
  });
  
  async function asyncCall() {
    console.log('Inside asyncCall method');
    var result = await resolveAfter2Seconds();
    console.log('result after await:', result);
    // expected output: 'resolved'
  }
  
  asyncCall();
// using arguments
function function1() {
    console.log('arguments:');
    console.log(arguments)
    console.log(arguments.length); // 3
    // but arguments were array like and not actual array
//    arguments.forEach((val)=>{
//     console.log(val);
//    });
  }

function1(1, 2, 3);
console.log('-------Rest parameters-------');
//rest parameters using  three dots
function greet( firstName, ...args) {
   console.log("Hello",firstName);
    console.log('rest parameters:');
    console.log(args);
    args.forEach((arg) => { console.log(arg); });
}
greet("Everyone", "Hope", "you", "like", "this!");
  
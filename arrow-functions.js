function circleArea1(r) {
    const PI = 3.14;
    return PI * r * r;
}

const circleArea2 = (r) =>{
    const PI = 3.14;
    return PI * r * r;
}

const circleArea3 = r => 3.14 * r * r;
console.log(circleArea1(7));
console.log(circleArea2(7));
console.log(circleArea3(7));

// Until arrow functions, every new function defined its own 'this' value
function Person1() {
    let that = this;
    this.age = 0;
  
    setTimeout(function growUp() {
      // The callback refers to the `that` variable of which
      // the value is the expected object.
      console.log('----Normal function------');
      console.log('this.age',this.age);
      that.age++;
      console.log('that.age',that.age);
    }, 1000);
}
const p1 = new Person1();

/**
 * An arrow function does not have its own this;
 * the this value of the enclosing lexical context is used i.e.
 * Arrow functions follow the normal variable lookup rules. 
 * So while searching for this  which is not present in current scope
 * they end up finding this from its enclosing scope . 
 *  */

function Person2(){
    this.age = 0;
  
    setTimeout(() => {
        console.log('----Arrow function------');
        this.age++; // |this| properly refers to the Person object
        console.log(this.age);
    }, 1000);
}
  
  const p2 = new Person2();
// destructures an array into individual parts
let arr1 = [0, 1, 2];
let arr2 = [...arr1, 3, 4, 5];
console.log('arr2 using normal spread syntax:', arr2); // prints [0,1,2,3,4,5]

// another imp usecase
// pushing in array of parameters
function addThreeThings( a, b, c){
    console.log('----Inside AddThreeThings----');
    let result = a + b + c;
    console.log('result:',result); // 6
  }
  addThreeThings(...arr1);

  arr2.push(...arr1);
  console.log('arr2 after using push function:',arr2);
ES6 most used features

1. Let + Const
	- var declared variables were not blocked scope but function scoped
	- let & const Block-scoped binding constructs
	- let is the new var. const is single-assignment.


2. Default function parameters:
	- allow named parameters to be initialized with default values if no value or undefined is passed.

3. Rest parameters to function:
	- Initially in ES5 passing  various parameters  in function was handled using built-in arguments parameter.
	- The rest parameter syntax allows us to represent an indefinite number of arguments as an array.

4.  Spread operator for arrays, strings and objects:
 It allows an iterable such as an array expression or string to be expanded in places where zero or more arguments (for function calls) or elements (for array literals) are expected, or an object expression to be expanded in places where zero or more key-value pairs (for object literals) are expected.

5.  Destructing assignment:
	- It is a JavaScript expression that makes it possible to unpack values from arrays, or properties from objects, into distinct variables.
6. Template literals: 
	- instead concatenating string using +, can now be done using`
	- can use multi-line strings 
	- allow embedded expressions

7. Classes:  
	- simple sugar over the prototype-based OO pattern
	- support prototype-based inheritance, super calls, instance and static methods and constructors.

8. Arrow functions 
 	-  has a shorter syntax than a function expression.

9. Enhance Object Literals

10. Promises : promise object represents the eventual completion (or failure) of an asynchronous operation, and its resulting value.

ES7 features:
1. Array.prototype.includes : bit like indexOf but instead returns a true or false value instead of the item's index.
2.  Exponentiation
3.  Object.values
4. Async/ Await over promises :  promise chain is better using async await
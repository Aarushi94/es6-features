// Assigning the rest of an array to a variable
let a, b, rest;
[a, b, ...rest] = [10, 20, 30, 40, 50];
console.log('a:', a); // 10
console.log('b:', b); // 20
console.log('rest:', rest); // [30, 40, 50]

// Assignment separate from declaration
let first, second;
[first, second] = [10, 20, 30, 40, 50];
console.log('first:', first); // 10
console.log('second:', second); // 20
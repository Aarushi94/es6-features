const firstName = 'aarushi';
const lastName = 'gupta';
function getName(){
    return `${firstName} ${lastName}`
}

console.log('person1 without object enhancements');
const person1 = {
    firstName:firstName,
    lastName:lastName,
    getName: getName
};
console.log(person1);

const person2 = {firstName, lastName, getName};
console.log('person2 with object enhancements');
console.log(person2);

// basic assignment
const obj = {p: 42, q: true};
const {p, q} = obj;
console.log('p:',p);
console.log('q:',q);

// Assignment without declaration
let firstName, lastName;
({firstName, lastName} = {firstName: 'aarushi', lastName:'gupta'});
console.log('firstName:',firstName);
console.log('lastName:',lastName);

// Nested object and array destructuring
const metadata = {
    title: 'Scratchpad',
    translations: [
       {
        locale: 'en',
        localization_tags: [],
        url: '/en/docs/Scratchpad',
        title: 'JavaScript'
       }
    ],
    url: '/docs/Scratchpad'
};

const {
    title: englishTitle, //Assigning to new variable names
    translations: [{title: localeTitle}]
    } = metadata;

console.log('englishTitle:', englishTitle); // "Scratchpad"
console.log('localeTitle:', localeTitle);  // "JavaScript"
console.log('-----------------------');

// Destructing parameters in method call
function displayDetails({title, url}){
    console.log('inside function')
    console.log('title:',title);
    console.log('url:',url);
}

displayDetails(metadata);
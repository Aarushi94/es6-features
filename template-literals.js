let a = 5;
let b = 10;
console.log('Fifteen is ' + (a + b) + ' and\nnot ' + (2 * a + b) + '.');

console.log('---- Template Literals------');
// Embedded expression and multiline
console.log(`Fifteen is ${a + b} and
not ${2 * a + b}.`);
var message = "outside block";
{
  var message = "inside block";
}
console.log('var message:',message);

// for let
let message1 = 'outside block';
{
   let message1 = 'inside block';
}
console.log('let message1:',message1);

//for const & let
function f() {
    {
      let x;
      // function scoping of var
      var message = 'Inside function';
      {
        // okay, block scoped name
        const x = "sneaky";
        // error, const
       // x = "foo";
      }
      // error, already declared in block
     // let x = "inner";
    }
  }

f();
console.log('message after function scoping:', message);
